from django.http import HttpResponse
from django.shortcuts import render
from lists.models import user

def home_page(request):
    usercode = user()
    usercode.code = request.POST.get('text_code', '')
    
    return render(request, 'home.html', {
    'text_show_new' : usercode.code,
})
