from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest, time

class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):

        self.browser.get('http://localhost:8000')

        self.assertIn('Procweb', self.browser.title)
        codebox = self.browser.find_element_by_id('text_code')
        codebox.send_keys("Hello")
        submit = self.browser.find_element_by_id('send')
        submit.click()
        
        time.sleep(5)


if __name__ == '__main__':
    unittest.main(warnings='ignore')
